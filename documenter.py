import os
import cadquery as cq
import parameters as params
from components.body import body
from components.shims.victure_hc200_shims import shim
from components.aperture_plate import plate


def document(base_dir, functions):
    # SVG visible and hidden line colors
    svg_line_color = (10, 10, 10)
    svg_hidden_color = (127, 127, 127)

    # Create the docs/images directory if it does not exist
    docs_images_path = os.path.join(base_dir, "docs", "images", "generated")
    exists = os.path.exists(docs_images_path)
    if not exists:
        os.makedirs(docs_images_path)

    # Create the docs/output/stl directory if it does not exist
    docs_output_path = os.path.join(
        base_dir, "docs", "manufacturing_files", "generated"
    )
    exists = os.path.exists(docs_output_path)
    if not exists:
        os.makedirs(docs_output_path)

    # Generate and export the tube body that holds the stereoscope hole plate
    bd = body(params)
    cq.exporters.export(
        bd, os.path.join(docs_output_path, "body.stl"), opt={"ascii": True},
    )
    cq.exporters.export(
        bd, os.path.join(docs_output_path, "body.step"),
    )

    # Generate and export the shim that goes between the camera/phone/trap body and the stereoscope
    sh = shim()
    cq.exporters.export(
        sh,
        os.path.join(docs_output_path, "victure_hc200_shim.stl"),
        opt={"ascii": True},
    )
    cq.exporters.export(
        sh, os.path.join(docs_output_path, "victure_hc200_shim.step"),
    )

    # Generate the aperture plate and export it to the formats that are required to cut it
    pl = plate(params)
    cq.exporters.export(
        pl,
        os.path.join(docs_images_path, "aperture_plate.svg"),
        opt={
            "width": 800,
            "height": None,
            "marginLeft": 10,
            "marginTop": 10,
            "showAxes": False,
            "projectionDir": (0.0, 0.0, 1.0),
            "strokeWidth": 0.5,
            "strokeColor": svg_line_color,
            "hiddenColor": svg_hidden_color,
            "showHidden": False,
        },
    )
    cq.exporters.export(
        pl, os.path.join(docs_output_path, "aperture_plate.dxf"),
    )


if __name__ == "__main__":
    document(".", None)
