from math import sqrt
import cadquery as cq

# All units in millimeters unless otherwise stated.

def build(params=None):
    """
    Generates the aperture plate the stereo images come through.
    """

    # Handle default parameters
    if params is None:
        params = {}
    if "plate_diameter" not in params:
        params["plate_diameter"] = 25.0
    if "focal_length" not in params:
        params["focal_length"] = 25.0
    if "slip_fit_dia_tolerance" not in params:
        params["slip_fit_dia_tolerance"] = 0.2
    if "aperture_plate_thickness" not in params:
        params["aperture_plate_thickness"] = 1.016
    if "alignment_notch_dia" not in params:
        params["alignment_notch_dia"] = 2.0

    # aperture_separation = params.diameter - 5.0  # mm
    aperture_dia =  0.0366 * sqrt(
        params["focal_length"]
    )  # mm, see following for equation: https://en.wikipedia.org/wiki/Pinhole_camera

    # The main outline of the plate
    pl = (
        cq.Workplane()
        .circle((params["plate_diameter"] - params["slip_fit_dia_tolerance"]) / 2.0)
        .extrude(params["aperture_plate_thickness"])
    )

    # The aperture holes in the plate
    pl = (
        pl.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        # .pushPoints(
        #     [
        #         (aperture_separation / 2.0, 0.0),
        #         (-aperture_separation / 2.0, 0.0),
        #     ]
        # )
        .hole(aperture_dia)
    )

    # Add alignment notch to plate
    pl = (
        pl.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .polarArray((params["plate_diameter"] - params["slip_fit_dia_tolerance"]) / 2.0, 0, 360, 4)
        .hole(params["alignment_notch_dia"] + params["slip_fit_dia_tolerance"])
    )

    return pl
