import cadquery as cq


def shim():
    """
    Generates a shim to adapt the stereoscope body to a
    Victure HC200.
    """

    # Overall shape
    sh = cq.Workplane().box(50.0, 43.0, 2.0)

    # Cutout for lens
    sh = (
        sh.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .center(0.0, -2.5)
        .circle((25.0 / 2))
        .cutBlind(2.0)
    )

    # Cutout for detail above lens
    sh = (
        sh.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 14.125)
        .rect(25.75 + 0.25, 4.25 + 0.25)
        .cutThruAll()
    )

    # Raised sections to attach stereoscope via bolt
    sh = (
        sh.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(19.0, 10.0), (-19.0, 10.0), (19.0, -15.0), (-19.0, -15.0)])
        .rect(10.0, 5.0)
        .extrude(10.0)
    )

    # Holes to accept M3 bolt
    sh = (
        sh.faces("<Y[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(19.0, 0.0), (-19.0, 0.0)])
        .hole(3.4)
    )

    # Rubber band hooks to secure the stereoscope to the camera trap
    sh = (
        sh.faces("<Z[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(10.0, 19.0), (-10.0, 19.0), (10.0, -19.0), (-10.0, -19.0)])
        .rect(5.0, 3.0)
        .extrude(5.0)
    )

    return sh
