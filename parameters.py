from math import sqrt

# Direct parameters
slip_fit_dia_tolerance = 0.2  # mm
focal_length = 25.0  # mm
aperture_plate_thickness = 1.016  # mm
camera_lens_dia = 25.0  # mm
alignment_notch_dia = 2.0  # mm

# Derived parameters
aperture_separation = camera_lens_dia - 5.0  # mm
aperture_dia = 0.0366 * sqrt(
    focal_length
)  # mm, see following for equation: https://en.wikipedia.org/wiki/Pinhole_camera
body_od = camera_lens_dia + 3.0  # mm
body_id = camera_lens_dia
