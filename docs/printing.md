[Filament]:Parts.yaml#Filament
[3D Printer]:Tools.yaml#3DPrinter

# Printing

{{BOM}}

## Download the STL Files {pagestep}

The STL files can be downloaded from the source repository [here](https://codeberg.org/7BIndustries/pinhole-stereoscope/docs/manufacturing_files). Download both the `body.stl` and `victure_hc200_shim.stl` files unless you have designed your own shim. If you have designed your own shim, just download the `body.stl` file.

## Print the Parts {pagestep}

1. Body ([preview](manufacturing_files/body.stl){previewpage}, [download](manufacturing_files/body.stl), [step file](manufacturing_files/body.step)) - This is the body of the stereoscope that holds the plate with the two pinholes in it so that two images can be captured. Approximately 2.6 meters of [Filament]{Qty: "2.6 m", Cat: part} will be required if 1.75 mm diameter filament is used. Use a [3D Printer]{Cat: tool, Qty: 1}.
    * Material: PETG (PLA should work fine as well, but will not be as durable)
    * Layer height: 0.15 mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2 (4 will add strength)
    * Supports: None
2. Shim ([preview](manufacturing_files/victure_hc200_shim.stl){previewpage}, [download](manufacturing_files/victure_hc200_shim.stl), [step file](manufacturing_files/victure_hc200_shim.step)) - The adapter that goes between the camera trap and the stereoscope body. This has mounting features to attach the body via screws and square nuts. Approximately 2.0 meters of [Filament]{Qty: "2.0 m", Cat: part} will be required if 1.75 mm diameter filament is used. 
    * Material: PETG (PLA should work fine as well, but will not be as durable)
    * Layer height: 0.15 mm
    * Infill: 20%
    * Brim: Yes
    * Perimeters: 2 (4 will add strength)
    * Supports: None
