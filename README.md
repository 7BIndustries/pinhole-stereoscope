# pinhole-stereoscope

[![status-badge](https://ci.codeberg.org/api/badges/12488/status.svg)](https://ci.codeberg.org/repos/12488)

**PLEASE NOTE:** This design was abandoned because pinhole cameras require manual adjustment of the f-number and exposure time. Neither of these adjustments is available in most camera traps, and even if they were, manual adjustment for different lighting conditions is not practical in the field. If new information and/or understanding is found, this project could always be restarted.

An experimental design to see if it is practical to create a stereoscope using the same "camera obscura" effect that makes pinhole cameras work.

Spin-off from the [poc-stereoscope](https://codeberg.org/7BIndustries/poc-stereoscope) design. Hopefully it will prove to be a more compact design, but pinhole cameras can have problems with blurring.

![Pinhole stereoscope CAD](docs/images/stereoscope_gallery.png)

## Licenses

* Software - [LGPL-3.0-or-later](LICENSES/LGPL-3.0-or-later.txt)
* Hardware - [CERN-OHL-P-2.0](LICENSES/CERN-OHL-P-2.0.txt)
* Documentation - [CC-BY-SA-4.0](LICENSES/CC-BY-SA-4.0.txt)
