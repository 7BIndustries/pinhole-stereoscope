## Experimental Design

### Introduction

This is a lab note that covers the effort to build a pinhole stereoscopic attachment for camera traps. This is a spin-off project of the [proof of concept stereoscope](https://7bindustries.com/hardware/stereoscope/v1/index.html).

### Motivation

The [proof of concept stereoscope](https://7bindustries.com/hardware/stereoscope/v1/index.html) does produce stereoscopic images, but is large, and in some cases, covers important parts of the camera trap's front such as the PIR sensor or IR LED grid. The hope is that a smaller form factor can be found that provides images that are acceptable.

### Initial Questions

* Will this type of stereoscope even work with a camera trap's lens and camera setup?
* How will the pinhole stereoscope perform in night vision scenarios?
* Will the holes in the aperture plate have enough distance between them for stereoscopic processing to be done on the resulting image(s)?

### Experimental Process

The first step is to create a pinhole camera attachment to see if a single image can be focused on the camera trap's camera sensor. There are some equations determining the diameter of the pinhole, and the pinhole to film distance, which can be found [here](https://en.wikipedia.org/wiki/Pinhole_camera#Selection_of_pinhole_size). An aperture plate with the correct diameter hole needs to be designed. A body to hold the plate at the correct distance from the camera trap lens needs to also be designed.

### Aperture Plate Design

A typical camera trap lens diameter is around &[25.0]{name:plate_diameter, units:mm}, and so that will be used as a starting point for the size of the aperture plate and body tube diameter. The aperture plate needs to have alignment notches inside of it which will match with tabs inside the body tube.

The equation describing the relationship between the aperture hole diameter (`d`) and the distance-to-film value (`f`) is shown below.

&& { d = 0.0366 * \sqrt(f) } &&

If this ideas makes it to a final design where two pinholes are used to produce a stereo image, there should probably be a second set of notches 90 degrees from the first so that the pinholes can be set in either a "horizontal" or a "vertical" orientation. Those will be added in this initial version for completeness. A preview of the aperture plate can be seen below.

@[Sample Model](./models/aperture_plate.py)
