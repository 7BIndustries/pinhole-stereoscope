import argparse
import cadquery as cq
from documenter import document
import parameters as params
from components.body import body
from components.aperture_plate import plate
from components.cameras.victure_hc200 import camera
from components.shims.victure_hc200_shims import shim


def build_stereo_assembly(annotate=None, step=None):
    """
    Builds the assembly representing all the parts of the stereoscope.
    """

    # The overall stereoscope assembly
    assy = cq.Assembly()
    stereo_assy = cq.Assembly()

    # Add the body tube of the stereoscope
    stereo_assy.add(
        body(params),
        name="body",
        loc=cq.Location((0.0, 0.0, 0.0)),
        color=cq.Color(0.8, 0.0, 0.05, 1),
    )

    # Add the aperture plate of the stereoscope
    stereo_assy.add(
        plate(params),
        name="plate",
        loc=cq.Location((0.0, 0.0, params.focal_length), (0, 0, 1), -90),
        color=cq.Color(0.04, 0.5, 0.67, 1),
    )

    # # Add the camera
    assy.add(
        camera(), name="camera", color=cq.Color(0.13, 0.545, 0.13, 1.0),
    )

    # Add the stereoscope sub-assembly
    assy.add(
        stereo_assy,
        name="stereo_assy",
        loc=cq.Location((0.0, -2.5, 15.0), (0, 0, 1), 90),
    )

    # Add the shim between the camera and the stereoscope
    assy.add(
        shim(),
        name="shim",
        loc=cq.Location((0.0, 0.0, 14.0)),
        color=cq.Color(1.0, 0.309, 0.0, 1.0),
    )

    return assy


# Show the object in the viewer if this script is being run by CQ-editor
if "show_object" in globals():
    # Build and show the assembly in CQ-editor
    assy = build_stereo_assembly()
    show_object(assy)


def main(args):
    # Determine whether the user wants to generate documentation/output, or display the model
    if args.document == True:
        functions = {}
        functions["build_stereo_assembly"] = build_stereo_assembly

        document(".", functions)
    else:
        from cadquery.vis import show

        # The stereoscope assembly
        stereo_assy = build_stereo_assembly()

        # if explode:
        #     explode_assembly(stereo_assy)

        # if annotate:
        #     add_assembly_arrows(stereo_assy, arrow_scale_factor=0.5)

        show(stereo_assy)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Displays and/or generates documentation and output files for this model."
    )
    parser.add_argument(
        "--document",
        dest="document",
        action="store_true",
        help="Tells the app whether or not to generate documentation and output files. If present, the model will not be displayed in a window.",
    )
    args = parser.parse_args()

    main(args)
