import cadquery as cq

# All units in millimeters unless otherwise stated.


def plate(params):
    """
    Generates the aperture plate the stereo images come through.
    """

    # The main outline of the plate
    pl = (
        cq.Workplane()
        .circle((params.body_id - params.slip_fit_dia_tolerance) / 2.0)
        .extrude(params.aperture_plate_thickness)
    )

    # The aperture hole in the plate
    pl = (
        pl.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .hole(params.aperture_dia)
    )

    # Add alignment notch to plate
    pl = (
        pl.faces(">Z")
        .workplane(centerOption="CenterOfBoundBox")
        .moveTo(0.0, (params.body_id - params.slip_fit_dia_tolerance) / 2.0)
        .hole(params.alignment_notch_dia + params.slip_fit_dia_tolerance)
    )

    return pl


def main():
    from cadquery.vis import show

    # Make sure we can find the parameters file in the top level directory
    import path
    import sys

    directory = path.Path(__file__).abspath()
    sys.path.append(directory.parent.parent)
    import parameters as params

    # Generate and show the model
    pl = plate(params)
    show(pl)


if __name__ == "__main__":
    main()
