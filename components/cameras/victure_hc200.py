import cadquery as cq

# Parameters
camera_height = 130.15
camera_width = 79.5
camera_lens_top = 74.75


def camera():
    """
    Creates a rough approximation of a Victure HC200 hunting camera so that
    a proper fit can be created between it, the camera shim and the sterescope.
    """

    # The camera front shape
    camera = cq.Workplane().rect(camera_width, camera_height).extrude(13.0)

    # Tag the face that we want to put all the features on
    camera.faces(">Z").tag("front_face")

    # Camera lens boss
    camera = (
        camera.faces(tag="front_face")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, -2.5)
        .circle(25.0 / 2.0)
        .extrude(2.0)
    )

    # Camera lens inset
    camera = (
        camera.faces(">Z")
        .workplane(invert=True, centerOption="CenterOfBoundBox")
        .circle(22.5 / 2, 0)
        .cutBlind(0.6)
    )

    # IR grid boss
    camera = (
        camera.faces(tag="front_face")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 39.685)
        .rect(55.25, 35.25)
        .extrude(1.5)
    )

    # The PIR sensor
    camera = (
        camera.faces(tag="front_face")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, -39.95)
        .rect(36.25, 33.5)
        .extrude(7.0)
    )

    # Detail above the lens
    camera = (
        camera.faces(tag="front_face")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 14.125)
        .rect(25.75, 4.25)
        .extrude(3.25)
    )

    # Add the top closure flanges
    camera = (
        camera.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(84.75, 136.15)
        .extrude(9.75)
    )

    # Add step below closure flanges
    camera = (
        camera.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(79.5, 131.0)
        .extrude(8.5)
    )

    # Next step
    camera = (
        camera.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .center(0.0, 4.25 / 2.0)
        .rect(74.4, 126.75)
        .extrude(2.5)
    )

    # Last step of back
    camera = (
        camera.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox")
        .rect(64.15, 126.15)
        .workplane(offset=10.75)
        .rect(60.15, 126.15)
        .loft()
    )

    return camera
