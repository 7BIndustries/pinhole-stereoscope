import cadquery as cq

# All units in millimeters unless otherwise stated.


def body(params):
    """
    Generates the body of the pinhole stereoscope, which is mainly just
    a tube to hold the pinhole plate.
    """

    # Main tube shape of the body
    body = (
        cq.Workplane()
        .circle(params.body_od / 2.0)
        .extrude(params.focal_length + (params.aperture_plate_thickness / 2.0))
    )

    # Mounting ears
    body = (
        body.faces("<Z")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .pushPoints(
            [(0.0, params.body_od / 2.0 + 2.5), (0.0, -params.body_od / 2.0 - 2.5)]
        )
        .rect(20.0, 15.0)
        .extrude(10.0)
    )

    # Center hole of the tube
    body = body.faces("<Z").circle(params.body_id / 2.0).cutThruAll()

    # Step to hold the pinhole plate
    body = (
        body.faces(">Z")
        .workplane()
        .circle(params.body_od / 2.0)
        .circle(params.body_id / 2.0 - 1.5)
        .extrude(2.0)
    )

    # Circular "notch" to orient the pinhole plate
    body = (
        body.faces(">Z[-2]")
        .workplane()
        .center(params.body_id / 2.0, 0.0)
        .circle(params.alignment_notch_dia / 2.0)
        .extrude(5.0)
    )

    # Holes in mounting ears for square nuts
    body = (
        body.faces(">X[-2]")
        .workplane(centerOption="CenterOfBoundBox")
        .pushPoints([(19.0, 0.0), (-19.0, 0.0)])
        .hole(3.4)
    )

    # Add square nut traps for attaching stereoscope to shim
    body = (
        body.faces(">Z[-4]")
        .workplane(centerOption="CenterOfBoundBox", invert=True)
        .pushPoints([(7.0, 19.0), (7.0, -19.0), (-7.0, 19.0), (-7.0, -19.0)])
        .rect(2.75, 5.75)
        .extrude(9.0, combine="cut")
    )

    return body


def main():
    from cadquery.vis import show

    # Make sure we can find the parameters file in the top level directory
    import path
    import sys

    directory = path.Path(__file__).abspath()
    sys.path.append(directory.parent.parent)
    import parameters as params

    # Generate and show the model
    bd = body(params)
    show(bd)


if __name__ == "__main__":
    main()
